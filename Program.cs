﻿using System;
using System.Collections.Generic;

namespace saltHelper
{
    class Program
    {
        static void Main(string[] args)
        {

            bool STOP = new bool();
            STOP = true;

            List<string> HASH_LIST = new List<string>();

            while(STOP)
            {

            Console.WriteLine("1 - rejestracja, 2 - logowanie, 3 - lista hashy");
            string INPUT = Console.ReadLine();

                switch (INPUT)
                {
                    case "1":
                        Console.WriteLine("Zarejestruj");

                        var REGISTER_INPUT = Console.ReadLine();

                        var SALTED_REGISTER_INPUT = PasswordHelper.CreateHash(REGISTER_INPUT);

                        HASH_LIST.Add(SALTED_REGISTER_INPUT);
                        break;

                    case "2":

                        Console.WriteLine("Problem jest taki, że nie mamy użytkownikow itp, a nie chce mi sie tego ogarniac, wiec iterujemy wszystko, szukamy takiego takiego samego hasla");
                        Console.WriteLine("zaloguj sie haslem");
                        string LOGIN_INPUT = Console.ReadLine();

                        //var SALTED_LOGIN_INPUT =
                        foreach(var HASH in HASH_LIST)
                        {
                            var HASH_COMPARER = PasswordHelper.ValidatePassword(LOGIN_INPUT, HASH);

                            if (HASH_COMPARER)
                            {

                                Console.WriteLine($"HASH : {HASH}, HASLO : {LOGIN_INPUT}");
                            }
                        }

                        break;

                    case "3":

                        foreach (var HASH in HASH_LIST)
                        {

                            Console.WriteLine(HASH);
                        }

                        break;

                    default:
                        Console.WriteLine(" default case");
                    break;
                }

                Console.WriteLine("Jesli chcesz przerwac wpisz 0");
                string broker = Console.ReadLine();

                if(broker == "0")
                {
                    STOP = false;
                }

            }
        }
    }
}
